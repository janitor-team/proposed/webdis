webdis (0.1.9+dfsg-1) unstable; urgency=medium

  * d/copyright: acknowledge upstream files relocation
  * New upstream version 0.1.9+dfsg
  * Refresh patches
  * Update include locations
  * d/copyright: fix whitespace
  * d/test.sh: fix whitespace
  * d/webdis.service: do not fail on mkdir (Closes: #940558) Thanks to
    Emmanuel Benoît
  * d/control: bump Standards-Version to 4.5.0 (no changes)

 -- Andrii Senkovych <andrii@senkovych.com>  Thu, 23 Apr 2020 02:04:04 +0300

webdis (0.1.4+dfsg-2) unstable; urgency=medium

  * Post unittests to python3; Closes: #943271, #951624

 -- Sandro Tosi <morph@debian.org>  Fri, 10 Apr 2020 02:40:29 -0400

webdis (0.1.4+dfsg-1) unstable; urgency=medium

  * New upstream version 0.1.4+dfsg
  * d/control, d/copyright: update maintainers email and proper name spelling.
  * Raise compat level to 11.
  * d/control: remove explicit *-dbg package definition.
  * d/control: bump Standards-Version to 4.2.0.
  * d/control: update Vcs-* fields to point to salsa.d.o.
  * d/control: add Rules-Requires-Root header set to no.
  * d/control: add direct build dependency on pkg-config.
  * d/webdis.install: no need to explicitly install webdis binary.
  * d/rules: remove DESTDIR and CONFDIR vars setup by hand.
  * d/rules: avoid installing webdis.prod.json from upstream.
  * d/control, d/rules: remove trailing whitespace.
  * d/control: remote unnecessary Testsuite header.
  * d/copyright: use https protocol.
  * Add systemd service configuration.
  * d/patches/fix-test-new-libevent.patch: fix pubsub test when compiled w/
    libevent 2.1.x. (Closes: #869951, #901555)
  * d/rules: use /usr/share/dpkg/buildflags.mk to populate build flags.
  * Fix lintian typo check

 -- Andrii Senkovych <andrii@senkovych.com>  Sun, 26 Aug 2018 15:30:36 +0300

webdis (0.1.2+dfsg-2) unstable; urgency=medium

  * Update testsuite variable names. Fixup to sockstat
  * Remove dependency on netstat or sockstat to run the tests

 -- Andriy Senkovych <jolly_roger@itblog.org.ua>  Thu, 17 Nov 2016 00:46:01 +0200

webdis (0.1.2+dfsg-1) unstable; urgency=medium

  * New upstream version 0.1.2+dfsg
  * Move repository to collab-maint on alioth
  * Repack upstream tarball to remove hiredis, jansson and b64 copies
  * Drop copyright notices for removed libraries
  * Update Standards-Version to 3.9.8. No changes required
  * Raise compat level to 10
  * Add explicit dependency on lsb-base
  * Enable all hardening features during build

 -- Andriy Senkovych <jolly_roger@itblog.org.ua>  Thu, 10 Nov 2016 01:36:21 +0200

webdis (0.1.1-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with recent msgpack-c versions.  (Closes: #811343)
    + Use pkg-config to determine CFLAGS/LDFLAGS
      - Search for msgpackc library in both MA and non-MA directories to ease
        backports
    + Use different APIs for the RAW type, based on the version of msgpack-c

 -- James McCoy <jamessan@debian.org>  Thu, 21 Jul 2016 23:43:34 -0400

webdis (0.1.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Remove embedded code copies (hiredis, jansson) in the clean target
    to make sure not to use them; this notably fixes the FTBFS during
    the libhiredis0.10 → libhiredis0.13 transition (Closes: #785476).

 -- Cyril Brulebois <kibi@debian.org>  Tue, 14 Jul 2015 18:32:13 +0200

webdis (0.1.1-2) unstable; urgency=medium

  * Use correct license names in debian/copyright
  * Document hiredis and jansson libraries
  * Add autopkgtest support
  * Update Standards-Version to 3.9.6. No changes required

 -- Andriy Senkovych <jolly_roger@itblog.org.ua>  Fri, 24 Oct 2014 22:48:16 +0300

webdis (0.1.1-1) unstable; urgency=low

  * Initial release (Closes: #623528)

 -- Andriy Senkovych <jolly_roger@itblog.org.ua>  Mon, 04 Aug 2014 02:10:55 +0300
